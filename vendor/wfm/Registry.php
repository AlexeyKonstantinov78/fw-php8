<?php

namespace wfm;

class Registry
{
    use TSingleton;

    protected static array $properties = [];

    public function setProperty($name, $value) {
        self::$properties[$name] = $value;
    }

    /**
     * @return array
     */
    public function getPropertiy($name)
    {
        return self::$properties[$name] ?? null;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return self::$properties;
    }
}