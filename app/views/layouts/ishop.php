<?php
    use wfm\View;

/**
 * @var $this View
 */

?>
    <?php $this->get_part('parts/header'); ?>
    <?= $this->content ?>
    <?php $this->get_part('parts/footer'); ?>